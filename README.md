# Static Template
> Base structure to develop a basic page integrating nodejs technologies.

Folder structure

```
src/
    coffee/
        dit/
    js/
    pug/
    scss/
    ts/
        dist/
    vue/
public/
    css/
    fonts/
    images/
    js/
```

# Installation
- Clone the repository: `https://gitlab.com/EstevanTn/static-template.git`
- Install dependeces via npm `npm install`
- Start local server `npm run test` or `gulp`

# Author

- author: [EstevanTn](https://gitlab.com/EstevanTn)
- email: [tunaqui@outlook.es](mailto:tunaqui@outlook.es)
- website: [https://tunaqui.com](https://tunaqui.com)